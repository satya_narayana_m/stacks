/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package expressions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import stacks.AStack;

/**
 *
 * @author Satyanarayana Madala
 */
public class BalancedParens {
    public static void main(String[] args) throws FileNotFoundException {
        AStack<Character> parenStack;
    Scanner in = new Scanner(new File("expressions.txt"));
    String expression;
    while(in.hasNext()){
            parenStack = new AStack<Character>();
            expression = in.nextLine();
            String string ="";
            for(int i=0;i<expression.length();i++){
            if(expression.charAt(i) == '(')
            {
               parenStack.push(expression.charAt(i));
            }  
             else if(expression.charAt(i) == ')'){
                if(parenStack.isEmpty()){
                    string = "Trying to pop, but the stack is empty!";
                    break;
                }else {
                    parenStack.pop();
                }
            }  
        }
            System.out.print(expression+": ");
        if(parenStack.isEmpty() && string.isEmpty()){
            System.out.println("VALID");
        }else {
            System.out.println("INVALID");
            if(parenStack.isEmpty()){
                System.out.println(string);
            }else {
                System.out.println("Parsing complete, but the stack is not empty!");
            }
        }
            System.out.println("");
            
    }
    }
    
}
