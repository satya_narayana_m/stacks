/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stacks;

import java.util.ArrayDeque;

/**
 *
 * @author satyanarayana Madala
 */
public class AStack<E> {
    private ArrayDeque<E> myStack;
    public AStack() {
        myStack = new ArrayDeque<E>();
                
    }
    public void push(E element){
        myStack.push(element);

    }
    public E pop(){

        return myStack.pop();
    
    }
    public E peek(){
       
        return myStack.peek();
    }
    public int size(){
        return myStack.size();
    }
    public boolean isEmpty(){
        int length = myStack.size();
        if (length == 0){
            return true;
        }else {
            return false;
        }
    }
}
